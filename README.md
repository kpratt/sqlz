# SQLz

SQLz is a type-safe query builder intended to be used for implementing Data Access Objects.
The current implementation is only supporting postgres and far from complete.
Users are encouraged to contribute via pull request when missing functionality is found.


## Usage Example,

Note that the dao/service a nil initialized, and that sqlz makes use of sqlx transaction types.
Beyond this the library does not prefer any particular DAO implementation, though you may note
the authors preference for monadic scoping ;)

```golang
import _ "github.com/jmoiron/sqlx"

import (
	sqlx "github.com/jmoiron/sqlx"
	"bitbucket.org/kpratt/sqlz/pkg/sqlz"
)


var users = struct {
	id   sqlz.IntegerColumn
	name sqlz.StringColumn
} {
	id:   sqlz.PrimaryKey("users"),
	name: sqlz.VarChar(64, "users", "name"),
}

// tags should be of the form table.column
// this is nessacery so that columns don't conflict when attempting joins
type User struct {
	ID       int    `db:"users.id"`
	Name     string `db:"users.name"`
}

type UserDAO struct {
	service sqlz.DB
}

func (dao UserDAO) GetUserById(id int, tx *sqlx.Tx, f func(User)) {
	var user User
	dao.service.
		Get(users.name).  // get executes into a struct, select executes into a slice.
		Where(users.id.Eq(id)).
		Exec(tx, &user)
	f(user)
	return
}
```

## Incompleteness

A nil pointer derefrence is most likely an indication
that a particular column type or condition is not yet implemented.
IntegerColumn.Eq should provide a simple scaffold for any functionality you wish to add.

Pull requests will not be accepted unless they come with atleast 1 accompanying integration test and 2 unit tests.
Non-trivial features will obviously require greater vetting. 


