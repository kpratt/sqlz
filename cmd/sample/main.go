package sample

import _ "github.com/jmoiron/sqlx"

import (
	sqlx "github.com/jmoiron/sqlx"
	"bitbucket.org/kpratt/sqlz/pkg/sqlz"
)


var users = struct {
	id   sqlz.IntegerColumn
	name sqlz.StringColumn
} {
	id:   sqlz.PrimaryKey("users"),
	name: sqlz.VarChar(64, "users", "name"),
}

// tags should be of the form table.column
// this is nessacery so that columns don't conflict when attempting joins
type User struct {
	ID       int    `db:"users.id"`
	Name     string `db:"users.name"`
}

type UserDAO struct {
	service sqlz.DB
}

func (dao UserDAO) GetUserById(id int, tx *sqlx.Tx, f func(User)) {
	var user User
	dao.service.
		Get(users.name).  // get executes into a struct, select executes into a slice.
		Where(users.id.Eq(id)).
		Exec(tx, &user)
	f(user)
	return
}

func main() {



}
