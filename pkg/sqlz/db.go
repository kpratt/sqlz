package sqlz

import (
	"github.com/jmoiron/sqlx"
	"bytes"
	"log"
	"fmt"
)

type dbImpl struct {
	conn *sqlx.DB
}

func Wrap(db *sqlx.DB) DB {
	return dbImpl{
		conn: db,
	}
}

func (db dbImpl) Select(columns ...Column) QueryBuilder {
	return &queryBuilder{
		columns: columns,
		offset:  0,
		limit:   50,
		get:     false,
	}
}

func (db dbImpl) Get(columns ...Column) QueryBuilder {
	return &queryBuilder{
		columns: columns,
		offset:  0,
		limit:   1,
		get:     true,
	}
}

func (db dbImpl) Insert(columns ...Column) InsertionBuilder {
	return &insertionBuilder{
		columns: columns,
	}
}

func (db dbImpl) Update(...Column) UpdateBuilder {
	return nil
}

func (db dbImpl) Delete(id int, tx sqlx.Tx) {
}

type queryBuilder struct {
	columns    []Column
	conditions []Condition
	orderings  []Ordering
	offset     int
	limit      int
	get        bool
}

func (qb *queryBuilder) Where(conditions ...Condition) QueryBuilder {
	qb.conditions = conditions
	return qb
}

func (qb *queryBuilder) OrderBy(orderings ...Ordering) QueryBuilder {
	qb.orderings = orderings
	return qb
}

func (qb *queryBuilder) Offset(i int) QueryBuilder {
	qb.offset = i
	return qb
}

func (qb *queryBuilder) Limit(n int) QueryBuilder {
	qb.limit = n
	return qb
}

func (qb *queryBuilder) BuildQuery() (query string, params []interface{}) {
	buf := bytes.Buffer{}
	params = make([]interface{}, 0, 8)
	tables := make(map[string]bool)
	for _, column := range qb.columns {
		tb := column.TableName()
		if _, ok := tables[column.TableName()]; !ok {
			tables[tb] = true
		}
	}

	buf.WriteString("SELECT ")
	for i, column := range qb.columns {
		if i != 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(column.Name())
	}

	buf.WriteString(" FROM ")
	comma := false
	for tbl := range tables {
		if comma {
			buf.WriteString(", ")
		}
		comma = true
		buf.WriteString(tbl)
	}

	if len(qb.conditions) > 0 {
		buf.WriteString(" WHERE ")
		comma := false
		for _, condition := range qb.conditions {
			if comma {
				buf.WriteString(" AND ")
			}
			comma = true
			fragment := condition.String()
			for _, p := range condition.Params() {
				params = append(params, p)
			}
			buf.WriteString(fragment)
		}
	}

	if len(qb.orderings) > 0 {
		buf.WriteString(" ORDER BY ")
		comma := false
		for _, ordering := range qb.orderings {
			if comma {
				buf.WriteString(", ")
			}
			comma = true
			buf.WriteString(ordering.col.Name())
			buf.WriteString(" ")
			buf.WriteString(string(ordering.ord))
		}
	}

	buf.WriteString(" OFFSET ?  LIMIT ?")
	params = append(params, qb.offset)
	params = append(params, qb.limit)

	buf.WriteString(";")
	query = buf.String()

	// postgres param flag re-writing
	buf = bytes.Buffer{}
	pDex := 1
	base := 0
	i := 0
	for ; i < len(query); i++ {
		if query[i] == '?' {
			buf.WriteString(query[base:i])
			buf.WriteString(fmt.Sprintf("$%d", pDex))
			base = i + 1
			pDex += 1
		}
	}
	buf.WriteString(query[base:i])
	query = buf.String()

	return
}
func (qb *queryBuilder) Exec(tx *sqlx.Tx, into interface{}) (error) {
	query, params := qb.BuildQuery()
	log.Println("DB query: ", query)
	if qb.get {
		return tx.Get(into, query, params...)
	} else {
		return tx.Select(into, query, params...)
	}
}

type insertionBuilder struct {
	columns []Column
}

func (qb *insertionBuilder) Exec(tx sqlx.Tx, values ...interface{}) {
	buf := bytes.Buffer{}

	buf.WriteString("INSERT INTO ")
	buf.WriteString(qb.columns[0].TableName())
	buf.WriteString(" (")
	comma := false
	for _, column := range qb.columns {
		if comma {
			buf.WriteString(", ")
		}
		comma = true
		buf.WriteString(column.Name())
	}
	buf.WriteString(") ")

	buf.WriteString(" VALUES ")
	buf.WriteString("(")
	comma = false
	for _, col := range qb.columns {
		if comma {
			buf.WriteString(", ")
		}
		buf.WriteString(":")
		buf.WriteString(col.Name())
	}

	buf.WriteString(")")
	buf.WriteString(";")
}
