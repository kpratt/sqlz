package sqlz

import (
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"github.com/jmoiron/sqlx"
)

func Test_SimpleSelect(t *testing.T) {
	mockTest(t, func(mock sqlmock.Sqlmock, dbx *sqlx.DB, dbz DB) {
		var user User
		mock.ExpectBegin()
		mock.ExpectQuery(`SELECT users.id FROM users OFFSET \$1 LIMIT \$2;`).
			WithArgs( 0, 50)
		mock.ExpectCommit()
		tx, err := dbx.Beginx()
		if err != nil {
			t.Error(err)
			return
		}
		dbz.Select(users.ID).Exec(tx, &user)
		tx.Commit()
	})
}

func Test_MultiSelect(t *testing.T) {
	mockTest(t, func(mock sqlmock.Sqlmock, dbx *sqlx.DB, dbz DB) {
		var us []User
		mock.ExpectBegin()
		mock.
		ExpectQuery(`SELECT users.id, users.name FROM users OFFSET \$1 LIMIT \$2;`).
			WithArgs(0, 50).
			WillReturnRows(
			sqlmock.NewRows([]string{"users.id", "users.name"}).
				AddRow(1, "fred").
				AddRow(2, "bob"))
		mock.ExpectCommit()
		tx, err := dbx.Beginx()
		if err != nil {
			t.Error(err)
			return
		}
		err = dbz.Select(users.ID, users.Name).Exec(tx, &us)
		tx.Commit()
		if err != nil {
			t.Error(err)
			return
		}

		if len(us) != 2 {
			t.Errorf("Recieved %d rows, expected 2", len(us))
		}
	})
}

func Test_SelectWithParam(t *testing.T) {
	mockTest(t, func(mock sqlmock.Sqlmock, dbx *sqlx.DB, dbz DB) {
		var us []User
		mock.ExpectBegin()
		mock.
		ExpectQuery(`SELECT users.id FROM users WHERE users.name == \$1 OFFSET \$2 LIMIT \$3;`).
			WithArgs("fred", 0, 50).
			WillReturnRows(
			sqlmock.NewRows([]string{"users.id"}).
				AddRow(1))
		mock.ExpectCommit()
		tx, err := dbx.Beginx()
		if err != nil {
			t.Error(err)
			return
		}
		err = dbz.Select(users.ID).Where(users.Name.Eq("fred")).Exec(tx, &us)
		tx.Commit()
		if err != nil {
			t.Error(err)
			return
		}

		if len(us) != 1 {
			t.Errorf("Recieved %d rows, expected 1", len(us))
		}
	})
}

func Test_SelectWithMultipleParams(t *testing.T) {
	mockTest(t, func(mock sqlmock.Sqlmock, dbx *sqlx.DB, dbz DB) {
		var us []User
		mock.ExpectBegin()
		mock.
		ExpectQuery(`SELECT users.id FROM users WHERE users.name == \$1 AND users.password == \$2 OFFSET \$3 LIMIT \$4;`).
			WithArgs("fred", "cats", 0, 50).
			WillReturnRows(
			sqlmock.NewRows([]string{"users.id"}).
				AddRow(1))
		mock.ExpectCommit()
		tx, err := dbx.Beginx()
		if err != nil {
			t.Error(err)
			return
		}
		err = dbz.Select(users.ID).Where(users.Name.Eq("fred"), users.Password.Eq("cats")).Exec(tx, &us)
		tx.Commit()
		if err != nil {
			t.Error(err)
			return
		}

		if len(us) != 1 {
			t.Errorf("Recieved %d rows, expected 1", len(us))
		}
	})
}
