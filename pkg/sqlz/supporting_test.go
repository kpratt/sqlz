package sqlz

import (
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"github.com/jmoiron/sqlx"
)

var users = struct {
	ID       IntegerColumn
	Name     StringColumn
	Password StringColumn
}{
	ID:       PrimaryKey("users"),
	Name:     VarChar(64, "users", "name"),
	Password: VarChar(64, "users", "password"),
}

type User struct {
	ID       int    `db:"users.id"`
	Name     string `db:"users.name"`
	PassWord string `db:"users.password"`
}

func mockTest(t *testing.T, f func(sqlmock.Sqlmock, *sqlx.DB, DB)) {
	db, mock, err := sqlmock.New()
	mock.MatchExpectationsInOrder(true)
	if err != nil {
		t.Error(err)
		return
	}
	dbx := sqlx.NewDb(db, "postgres")
	dbz := Wrap(dbx)
	f(mock, dbx, dbz)
	// we make sure that all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Mock Expectation: %s", err)
	}
}
