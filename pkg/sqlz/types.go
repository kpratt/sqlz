package sqlz

import (
	"time"
	"github.com/jmoiron/sqlx"
)

type DB interface {
	Get(...Column) QueryBuilder
	Select(...Column) QueryBuilder
	Insert(...Column) InsertionBuilder
	Update(...Column) UpdateBuilder
	Delete(id int, tx sqlx.Tx)
}

type QueryBuilder interface {
	/**
	 * The root condition is conjunctive (ANDs)
	 */
	Where(...Condition) QueryBuilder
	OrderBy(...Ordering) QueryBuilder
	Offset(i int) QueryBuilder
	Limit(n int) QueryBuilder
	Exec(tx *sqlx.Tx, into interface{}) (error)
}

type InsertionBuilder interface {
	Exec(tx sqlx.Tx, values ...interface{})
}

type UpdateBuilder interface {
	Exec(sqlx.Tx, interface{})
}

type Order string

var (
	ASC  Order = "ASC"
	DESC Order = "DESC"
)

type Ordering struct {
	col Column
	ord Order
}

type Condition interface {
	String() (string)
	Params() []interface{}
}

type Column interface {
	Name() string
	TableName() string
	IsNull() Condition
	IsNotNull() Condition
}

type IntegerColumn interface {
	Eq(int) Condition
	NotEq(int) Condition
	GT(int) Condition
	LT(int) Condition
	GTE(int) Condition
	LTE(int) Condition
	Column
}

type StringColumn interface {
	Eq(string) Condition
	NotEq(string) Condition
	GT(string) Condition
	LT(string) Condition
	GTE(string) Condition
	LTE(string) Condition
	Like(string) Condition
	Column
}

type DateColumn interface {
	At(time.Time) Condition
	Before(time.Time) Condition
	After(time.Time) Condition
	BeforeInclusive(time.Time) Condition
	AfterInclusive(time.Time) Condition
	Column
}

type BooleanColumn interface {
	IsTrue() Condition
	IsFalse() Condition
	Column
}
