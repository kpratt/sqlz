package sqlz

import (
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"github.com/jmoiron/sqlx"
)

func Test_Mock(t *testing.T) {
	db, mock, err := sqlmock.New()
	mock.MatchExpectationsInOrder(true)
	if err != nil {
		t.Error(err)
		return
	}

	mock.ExpectQuery("SELECT users.id, users.name FROM users OFFSET 0 LIMIT 50;").
		WillReturnRows(
		sqlmock.NewRows([]string{"id", "name"}).
			AddRow(1, "fred").
			AddRow(2, "bob"))

	rows, err := db.Query("SELECT users.id, users.name FROM users OFFSET 0 LIMIT 50;")
	if err != nil {
		t.Error(err)
		return
	}

	if ! rows.Next() {
		t.Error("Did not recieve first row.")
		return
	}

	var user User
	err = rows.Scan(&user.ID, &user.Name)
	if err != nil {
		t.Error(err)
		return
	}

	if ! rows.Next() {
		t.Error("Did not recieve second row.")
		return
	}

	err = rows.Scan(&user.ID, &user.Name)
	if err != nil {
		t.Error(err)
		return
	}

	if user.ID != 2 {
		t.Error("User ID was not assigned.")
	}

	if user.Name != "bob" {
		t.Error("User Name was not assigned.")
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Mock Expectation: %s", err)
	}
}

func Test_MockWithSQLX(t *testing.T) {
	type User struct {
		ID   int `db:"users.id"`
		Name string `db:"users.name"`
	}
	var users []User

	db, mock, err := sqlmock.New()
	mock.MatchExpectationsInOrder(true)
	if err != nil {
		t.Error(err)
		return
	}
	dbx := sqlx.NewDb(db, "postgres")

	mock.ExpectQuery("SELECT users.id, users.name FROM users OFFSET 0 LIMIT 50;").
		WillReturnRows(
		sqlmock.NewRows([]string{"users.id", "users.name"}).
			AddRow(1, "fred").
			AddRow(2, "bob"))

	err = dbx.Select(&users, "SELECT users.id, users.name FROM users OFFSET 0 LIMIT 50;")
	if err != nil {
		t.Error(err)
		return
	}



	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Mock Expectation: %s", err)
	}
}
