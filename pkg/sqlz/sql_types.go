package sqlz

/**
 * The primary key is named id. Period.
 */
func PrimaryKey(table string) IntegerColumn {
	return Integer(table, "id")
}

/**
 * Assumes non-cyclic data structure.
 */
func ForeignKey(pkColumn IntegerColumn) IntegerColumn {
	return nil
}

func Date(table string, name string) DateColumn {
	return nil
}

func VarChar(length int, table string, name string) StringColumn {
	return varcharColumn{
		length:length,
		table: table,
		name: name,
	}
}

func Integer(table string, name string) IntegerColumn {
	return integerColumn {
		table: table,
		name: name,
	}
}

type integerColumn struct {
	table string
	name string
}

func (column integerColumn) Name() string {
	return column.table + "." + column.name
}

func (column integerColumn) TableName() string {
	return column.table
}

func (column integerColumn) IsNull() Condition {
	return nil
}

func (column integerColumn) IsNotNull() Condition {
	return nil
}

type integerEquality struct {
	str string
	param int
}

func (condition integerEquality) String() string {
	return condition.str
}

func (condition integerEquality) Params() []interface{} {
	return []interface{}{condition.param}
}


func (column integerColumn) Eq(x int) Condition {
	cond := integerEquality{
		str: (column.name + " == ?"),
		param: x,
	}
	return cond
}

func (column integerColumn) NotEq(int) Condition {
	return nil
}

func (column integerColumn) GT(int) Condition {
	return nil
}

func (column integerColumn) LT(int) Condition {
	return nil
}

func (column integerColumn) GTE(int) Condition {
	return nil
}

func (column integerColumn) LTE(int) Condition {
	return nil
}

type varcharColumn struct {
	table string
	name string
	length int
}

func (column varcharColumn) Name() string {
	return column.table +"."+ column.name
}

func (column varcharColumn) TableName() string {
	return column.table
}

func (column varcharColumn) IsNull() Condition {
	return nil
}

func (column varcharColumn) IsNotNull() Condition {
	return nil
}

func (column varcharColumn) Eq(x string) Condition {
	cond := varcharEquality{
		str: (column.Name() + " == ?"),
		param: x,
	}
	return cond
}

type varcharEquality struct {
	str string
	param string
}

func (condition varcharEquality) String() string {
	return condition.str
}

func (condition varcharEquality) Params() []interface{} {
	return []interface{}{condition.param}
}

func (column varcharColumn) NotEq(string) Condition {
	return nil
}

func (column varcharColumn) GT(string) Condition {
	return nil
}

func (column varcharColumn) LT(string) Condition {
	return nil
}

func (column varcharColumn) GTE(string) Condition {
	return nil
}

func (column varcharColumn) LTE(string) Condition {
	return nil
}

func (column varcharColumn) Like(string) Condition {
	return nil
}


